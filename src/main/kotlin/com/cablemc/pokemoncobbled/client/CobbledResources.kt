package com.cablemc.pokemoncobbled.client

import com.cablemc.pokemoncobbled.common.util.cobbledResource

object CobbledResources {
    /**
     * Textures
     */
    val RED = cobbledResource("textures/red.png")
    val PHASE_BEAM = cobbledResource("textures/phase_beam.png")

    /**
     * Fonts
     */
    val NOTO_SANS_BOLD = cobbledResource("notosans_bold")
    val NOTO_SANS_BOLD_SMALL = cobbledResource("notosans_bold_small")
    val NOTO_SANS_REGULAR = cobbledResource("notosans_regular_small")
}