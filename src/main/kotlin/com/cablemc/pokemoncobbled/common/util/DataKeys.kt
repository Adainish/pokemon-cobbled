package com.cablemc.pokemoncobbled.common.util

object DataKeys {
    const val POKEMON = "Pokemon"
    const val POKEMON_UUID = "UUID"
    const val POKEMON_SPECIES_DEX = "SpeciesDex"
    const val POKEMON_FORM_ID = "FormId"
    const val POKEMON_LEVEL = "Level"
    const val POKEMON_STATS = "Stats"
    const val POKEMON_IVS = "IVs"
    const val POKEMON_EVS = "EVs"
    const val POKEMON_HEALTH = "Health"
    const val POKEMON_SCALE_MODIFIER = "ScaleModifier"
    const val POKEMON_MOVESET = "MoveSet"
    const val POKEMON_MOVESET_MOVENAME = "MoveName"
    const val POKEMON_MOVESET_MOVEPP = "MovePP"
    const val POKEMON_MOVESET_MAXPP = "MaxPP"
    const val POKEMON_ABILITY_NAME = "Ability"
    const val POKEMON_SHINY = "Shiny"

    const val POKEMON_STATE = "State"
    const val POKEMON_STATE_TYPE = "StateType"
    const val POKEMON_STATE_SHOULDER = "StateShoulder"
    const val POKEMON_STATE_ID = "StateId"
    const val POKEMON_STATE_PLAYER_UUID = "PlayerUUID"

    const val STORE_SLOT = "Slot"
    const val STORE_SLOT_COUNT = "SlotCount"
    const val STORE_BOX = "Box"
    const val STORE_BOX_COUNT = "BoxCount"

    const val REQUEST_TYPE = "RequestType"
    const val REQUEST_BATTLE_ID = "RequestBattleId"
    const val REQUEST_BATTLE_START = "StartBattle"
    const val REQUEST_BATTLE_SEND_MESSAGE = "SendMessage"
    const val REQUEST_MESSAGES = "RequestMessages"

}