package com.cablemc.pokemoncobbled.common.api.pokemon

import com.cablemc.pokemoncobbled.common.pokemon.Gender

/** A grouping of typical, chooseable properties for a Pokémon. Species agnostic. */
class PokemonProperties {
    var gender: Gender? = null
    var level: Int? = null

}