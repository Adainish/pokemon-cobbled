package com.cablemc.pokemoncobbled.common.api.storage.party

import com.cablemc.pokemoncobbled.common.api.storage.StorePosition

data class PartyPosition(val slot: Int) : StorePosition